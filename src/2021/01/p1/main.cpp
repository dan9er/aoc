// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <cstdint>

int main()
{
    uint16_t x   = UINT16_MAX,
             y   = 0         ,
             res = 0         ;

    while (std::cin >> y)
    {
        if (x < y)
            ++res;
        x = y;
    }

    std::cout << res << std::endl;
}
