// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <vector>
#include <cstdint>

int main()
{
    uint16_t x   = UINT16_MAX,
             y   = 0         ,
             res = 0         ;
    std::vector<uint16_t> vec;

    {
        uint16_t i = 0;
        while (std::cin >> i)
            vec.push_back(i);
    }

    for
    (
        std::vector<uint16_t>::const_iterator i = vec.cbegin();
        i+2 != vec.cend();
        ++i
    )
    {
        y = *i + *(i+1) + *(i+2);
        if (x < y)
            ++res;
        x = y;
    }

    std::cout << res << std::endl;
}
