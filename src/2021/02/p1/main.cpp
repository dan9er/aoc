// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <string>

int main()
{
    int h = 0, d = 0, j;
    std::string i;

    while (std::cin >> i >> j)
    {
        switch (i.front())
        {
            case 'f':
                h += j;
            break;

            case 'd':
                d += j;
            break;

            case 'u':
                d -= j;
            break;
        }
    }

    std::cout << h * d << std::endl;
}
