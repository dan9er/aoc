// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <string>

int main()
{
    int h = 0, d = 0, a = 0, j;
    std::string i;

    while (std::cin >> i >> j)
    {
        switch (i.front())
        {
            case 'f':
                h += j    ;
                d += j * a;
            break;

            case 'd':
                a += j;
            break;

            case 'u':
                a -= j;
            break;
        }
    }

    std::cout << h * d << std::endl;
}
