// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <vector>
#include <bitset>

int main()
{
    std::vector<std::bitset<12>> vec;
    std::vector<unsigned> c(12, 0);
    std::bitset<12> e, g;

    vec.reserve(1000);
    std::bitset<12> i;
    while (std::cin >> i)
        vec.push_back(i);

    for (unsigned j = 0; j != 12; ++j)
    {
        for (std::bitset<12> k : vec)
            if (k[j])
                ++c[j];

        if (c[j] > 500)
        {
            e[j] = true;
            g[j] = false;
        }
        else
        {
            e[j] = false;
            g[j] = true;
        }
    }

    std::cout << e.to_ulong() * g.to_ulong() << std::endl;
}
