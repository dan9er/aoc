// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <vector>
#include <bitset>

constexpr size_t W = 12;

void erase_bit(std::vector<std::bitset<W>>& vec, const size_t pos, const bool val)
{
    for (auto l = vec.begin(); l != vec.end();)
    {
        if ((*l)[pos] == val)
        {
            l = vec.erase(l);
        }
        else
        {
            ++l;
        }
    }
}

int main()
{
    std::vector<std::bitset<W>> vec_o, vec_c;
    std::vector<int> c_o, c_c;
    std::bitset<W> o, c;

    vec_o.reserve(1000);
    std::bitset<W> i;
    while (std::cin >> i)
    {
        vec_o.push_back(i);
        vec_c.push_back(i);
    }

    for (int j = W-1; j != -1; --j)
    {
        c_o.assign(W, 0);
        for (std::bitset<W> k : vec_o)
        {
            if (k[j])
                ++c_o[j];
            else
                --c_o[j];
        }

        if (c_o[j] >= 0)
            erase_bit(vec_o, j, false);
        else
            erase_bit(vec_o, j, true);

        if (vec_o.size() == 1)
        {
            o = vec_o[0];
            break;
        }
    }

    for (int j = W-1; j != -1; --j)
    {
        c_c.assign(W, 0);
        for (std::bitset<W> k : vec_c)
        {
            if (k[j])
                ++c_c[j];
            else
                --c_c[j];
        }

        if (c_c[j] < 0)
            erase_bit(vec_c, j, false);
        else
            erase_bit(vec_c, j, true);

        if (vec_c.size() == 1)
        {
            c = vec_c[0];
            break;
        }
    }

    std::cout << o.to_ulong() * c.to_ulong() << std::endl;
}
