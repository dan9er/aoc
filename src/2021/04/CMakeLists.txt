# See COPYING.txt file for copyright and license details.

# parts
add_subdirectory("./p1")
add_subdirectory("./p2")

# meta target
add_custom_target(2021_04
    DEPENDS
        2021_04_p1
        2021_04_p2
)
