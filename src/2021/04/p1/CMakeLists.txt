# See COPYING.txt file for copyright and license details.

# create executable
add_executable(2021_04_p1 main.cpp)

set_target_properties(2021_04_p1 PROPERTIES
    # compile with ISO C++11
    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS OFF
    # place final executable into this folder instead of root build/
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    # name final executable "a(.exe)"
    RUNTIME_OUTPUT_NAME "a"
)
