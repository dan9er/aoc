// See COPYING.txt file for copyright and license details.

#include <bitset>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <vector>
#include <array>

std::vector<unsigned> read_num(std::istream& in)
{
    std::vector<unsigned> res;

    std::string i;
    std::getline(in, i);

    std::istringstream sin(i);
    char j[3];
    while (sin.getline(j,3,','))
        res.push_back(std::stoi(j));

    return res;
}

std::vector<std::pair<std::array<unsigned,25>,std::bitset<25>>> read_boards(std::istream& in)
{
    std::vector<std::pair<std::array<unsigned,25>,std::bitset<25>>> res;

    std::array<unsigned,25> i;
    while
    (
        in >> i[0]
           >> i[1]
           >> i[2]
           >> i[3]
           >> i[4]
           >> i[5]
           >> i[6]
           >> i[7]
           >> i[8]
           >> i[9]
           >> i[10]
           >> i[11]
           >> i[12]
           >> i[13]
           >> i[14]
           >> i[15]
           >> i[16]
           >> i[17]
           >> i[18]
           >> i[19]
           >> i[20]
           >> i[21]
           >> i[22]
           >> i[23]
           >> i[24]
    )
    {
        res.push_back({i,0});
    }

    return res;
}

bool check_board(const std::bitset<25>& b)
{
    /*
     0  1  2  3  4
     5  6  7  8  9
    10 11 12 13 14
    15 16 17 18 19
    20 21 22 23 24
    */

    if
    (
        (b.to_ulong() & 0x1f) == 0x1f ||
        (b.to_ulong() & 0x3e0) == 0x3e0 ||
        (b.to_ulong() & 0x7c00) == 0x7c00 ||
        (b.to_ulong() & 0xf8000) == 0xf8000 ||
        (b.to_ulong() & 0x1f00000) == 0x1f00000 ||
        (b.to_ulong() & 0x108421) == 0x108421 ||
        (b.to_ulong() & 0x210842) == 0x210842 ||
        (b.to_ulong() & 0x421084) == 0x421084 ||
        (b.to_ulong() & 0x842108) == 0x842108 ||
        (b.to_ulong() & 0x1084210) == 0x1084210 //||
        //(b.to_ulong() & 0x1041041) == 0x1041041 ||
        //(b.to_ulong() & 0x111110) == 0x111110
    )
        return true;
    else
        return false;
}

std::pair
<
    std::vector<unsigned>::const_iterator,
    std::vector<std::pair<std::array<unsigned,25>,std::bitset<25>>>::const_iterator
>
run_game
(
    const std::vector<unsigned>& num,
    std::vector<std::pair<std::array<unsigned,25>,std::bitset<25>>>& boards
)
{
    for (auto i = num.cbegin(); i != num.cend(); ++i)
    {
        for (auto j = boards.begin(); j != boards.end(); ++j)
        {
            for (size_t k = 0; k != 25; ++k)
                if (*i == (*j).first[k])
                    (*j).second[k] = true;

            if (check_board((*j).second))
                return {i,j};
        }
    }

    throw;
}

int main()
{
    std::vector<unsigned> num = read_num(std::cin);
    std::vector<std::pair<std::array<unsigned,25>,std::bitset<25>>> boards = read_boards(std::cin);

    auto win = run_game(num, boards);
    unsigned sum = 0;

    for (size_t i = 0; i != 25; ++i)
        if (!win.second->second[i])
            sum += win.second->first[i];

    std::cout << sum * (*win.first) << std::endl;
}
