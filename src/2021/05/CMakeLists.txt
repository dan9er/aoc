# See COPYING.txt file for copyright and license details.

# parts
add_subdirectory("./p1")
add_subdirectory("./p2")

# meta target
add_custom_target(2021_05
    DEPENDS
        2021_05_p1
        2021_05_p2
)
