// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <vcruntime.h>
#include <vector>
#include <array>

using Field = std::array<std::array<unsigned,999>,999>;
using Coord = std::pair<Field::size_type,std::array<unsigned,999>::size_type>;
using Pair = std::pair<Coord,Coord>;

std::vector<Pair> read_pairs(std::istream& in)
{
    std::vector<Pair> res;
    std::string i;
    std::string j;
    Coord p0, p1;

    while (std::getline(in, i))
    {
        std::istringstream i_in(i);

        std::getline(i_in, j, ',');
        p0.first = std::stoi(j);

        std::getline(i_in, j, ' ');
        p0.second = std::stoi(j);

        i_in.seekg(3, std::ios_base::cur);

        std::getline(i_in, j, ',');
        p1.first = std::stoi(j);

        std::getline(i_in, j);
        p1.second = std::stoi(j);

        if (p0.first == p1.first)
        {
            if (p0.second >= p1.second)
                res.push_back({p1,p0});
            else
                res.push_back({p0,p1});
        }
        else if (p0.second == p1.second)
        {
            if (p0.first >= p1.first)
                res.push_back({p1,p0});
            else
                res.push_back({p0,p1});
        }
        else
            continue;
    }

    return res;
}

int main()
{
    std::unique_ptr<Field> field(new Field);
    std::vector<Pair> pairs = read_pairs(std::cin);

    for (auto& x : *field)
        for (auto& y : x)
            y = 0;

    for (const auto& p : pairs)
    {
        if (p.first.first == p.second.first)
            for (auto i = p.first.second; i != p.second.second+1; ++i)
                ++(*field)[p.first.first][i];
        else if (p.first.second == p.second.second)
            for (auto i = p.first.first; i != p.second.first+1; ++i)
                ++(*field)[i][p.first.second];
    }

    unsigned res = 0;
    for (auto& x : *field)
        for (auto& y : x)
            if (y > 1)
                ++res;

    std::cout << res << std::endl;
}
