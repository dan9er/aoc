// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <vector>
#include <array>

using Field = std::array<std::array<unsigned,1000>,1000>;
using Coord = std::pair<int,int>;
using Pair = std::pair<Coord,Coord>;

std::vector<Pair> read_pairs(std::istream& in)
{
    std::vector<Pair> res;
    std::string i;
    std::string j;
    Coord p0, p1;

    while (std::getline(in, i))
    {
        std::istringstream i_in(i);

        std::getline(i_in, j, ',');
        p0.first = std::stoi(j);

        std::getline(i_in, j, ' ');
        p0.second = std::stoi(j);

        i_in.seekg(3, std::ios_base::cur);

        std::getline(i_in, j, ',');
        p1.first = std::stoi(j);

        std::getline(i_in, j);
        p1.second = std::stoi(j);

        res.push_back({p0,p1});
    }

    return res;
}

std::vector<Coord> get_line(const Pair& p)
{
    std::vector<Coord> res;
    std::vector<int> x, y;

    if (p.first.first > p.second.first)
        for (int i = p.second.first; i != p.first.first+1; ++i)
            x.push_back(i);
    else if (p.first.first < p.second.first)
        for (int i = p.second.first; i != p.first.first-1; --i)
            x.push_back(i);
    else
        x.push_back(p.first.first);

    if (p.first.second > p.second.second)
        for (int i = p.second.second; i != p.first.second+1; ++i)
            y.push_back(i);
    else if (p.first.second < p.second.second)
        for (int i = p.second.second; i != p.first.second-1; --i)
            y.push_back(i);
    else
        y.push_back(p.first.second);

    if (x.size() == 1)
        for (const auto& j : y)
            res.push_back({*x.cbegin(),j});
    else if (y.size() == 1)
        for (const auto& i : x)
            res.push_back({i,*y.cbegin()});
    else
    {
        auto i = x.cbegin(),
             j = y.cbegin();

        while (i != x.cend() && j != y.cend())
        {
            res.push_back({*i,*j});
            ++i; ++j;
        }
    }

    return res;
}

int main()
{
    std::unique_ptr<Field> field(new Field);
    std::vector<Pair> pairs = read_pairs(std::cin);

    for (auto& x : *field)
        for (auto& y : x)
            y = 0;

    for (const auto& p : pairs)
        for (const auto& c : get_line(p))
            ++(*field)[c.first][c.second];

    unsigned res = 0;
    for (auto& x : *field)
        for (auto& y : x)
            if (y > 1)
                ++res;

    std::cout << res << std::endl;
}
