// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <string>
#include <vector>

using School = std::vector<unsigned>;

School read_fish(std::istream& in)
{
    School ret;

    std::string i;
    while (std::getline(in, i, ','))
        ret.push_back(std::stoi(i));

    return ret;
}

int main()
{
    School fish = read_fish(std::cin);

    for (unsigned d = 0; d != 80; ++d)
    {
        unsigned n = 0;

        for (auto& f : fish)
        {
            if (f == 0)
            {
                f = 6;
                ++n;
            }
            else
                --f;
        }

        for (unsigned s = 0; s != n; ++s)
            fish.push_back(8);
    }

    std::cout << fish.size() << std::endl;
}
