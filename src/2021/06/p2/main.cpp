// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <string>
#include <array>

using School = std::array<unsigned long long,9>;

School read_fish(std::istream& in)
{
    School ret = {0,0,0,0,0,0,0,0,0};

    std::string i;
    while (std::getline(in, i, ','))
        ++ret[std::stoi(i)];

    return ret;
}

int main()
{
    School fish = read_fish(std::cin);

    for (unsigned d = 0; d != 256; ++d)
    {
        unsigned long long n = fish[0];

        for (size_t f = 0; f != 8; ++f)
            fish[f] = fish[f+1];

        fish[6] += n;
        fish[8]  = n;
    }

    unsigned long long ret = 0;
    for (auto& f : fish)
        ret += f;

    std::cout << ret << std::endl;
}
