// See COPYING.txt file for copyright and license details.

#include <array>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <vector>

using Digits = std::array<std::string,4>;

std::vector<Digits> read_notes(std::istream& in)
{
    std::vector<Digits> res;

    std::string i;

    while (std::getline(in, i))
    {
        std::istringstream i_in(i);

        std::getline(i_in, i, '|');

        Digits j;
        i_in >> j[0] >> j[1] >> j[2] >> j[3];

        res.push_back(j);
    }

    return res;
}

int main()
{
    std::vector<Digits> digits = read_notes(std::cin);
    unsigned res = 0;

    for (const auto& i : digits)
        for (const auto& j : i)
            switch (j.size())
            {
                case 2:
                case 3:
                case 4:
                case 7:
                    ++res;

                default: break;
            }

    std::cout << res << std::endl;
}
