// See COPYING.txt file for copyright and license details.

#include <algorithm>
#include <array>
#include <bitset>
#include <iostream>
#include <istream>
#include <sstream>
#include <string>
#include <set>
#include <utility>
#include <vector>

#define SEG(c) static_cast<unsigned char>(c - 'a')

using Data = std::bitset<7>;
using Line = std::array<Data,14>;
constexpr size_t DIGITS_OFF = 10;

struct Mapping
{
    std::array<Data,7> map;

    Data& operator[](const unsigned char& n) &
        {return map[SEG(n)];}
    const Data& operator[](const unsigned char& n) const &
        {return map[SEG(n)];}

    bool solved() const &
    {
        for (const auto& i : map)
            if (i.count() != 1)
                return false;
        return true;
    }
};

Data read_data(const std::string& s)
{
    Data d = 0;

    for (const char& c : s)
        d[SEG(c)] = true;

    return d;
}

Line read_line(std::istream& in)
{
    Line l;

    for (unsigned n = 0; n != 10; ++n)
    {
        std::string j;
        in >> j;
        l[n] = read_data(j);
    }

    in.ignore(std::numeric_limits<std::streamsize>::max(), '|');

    for (unsigned n = DIGITS_OFF; n != DIGITS_OFF+4; ++n)
    {
        std::string j;
        in >> j;
        l[n] = read_data(j);
    }

    return l;
}

std::vector<Line> read_notes(std::istream& in)
{
    std::vector<Line> res;

    std::string line;
    while (std::getline(in, line))
    {
        std::istringstream lin(line);
        res.push_back(read_line(lin));
    }

    return res;
}

int main()
{
    std::vector<Line> notes = read_notes(std::cin);
    unsigned long long res = 0;

    for (const Line& i : notes)
    {
        // m[segment] = bitmask of possible wires for segment
        Mapping m = {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};

        // d[digit] = bitmask of possible signal of digit
        std::array<Data, 10> d = {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};

        // look at signals w/ unique no. of segments
        for (const auto& j : i)
            switch (j.count())
            {
                case 2:
                    d[1] = j;
                    m['a'] &= ~j;
                    m['b'] &= ~j;
                    m['c'] &=  j;
                    m['d'] &= ~j;
                    m['e'] &= ~j;
                    m['f'] &=  j;
                    m['g'] &= ~j;
                break;

                case 3:
                    d[7] = j;
                    m['a'] &=  j;
                    m['b'] &= ~j;
                    m['c'] &=  j;
                    m['d'] &= ~j;
                    m['e'] &= ~j;
                    m['f'] &=  j;
                    m['g'] &= ~j;
                break;

                case 4:
                    d[4] = j;
                    m['a'] &= ~j;
                    m['b'] &=  j;
                    m['c'] &=  j;
                    m['d'] &=  j;
                    m['e'] &= ~j;
                    m['f'] &=  j;
                    m['g'] &= ~j;
                break;

                case 5:
                    d[2] &= j;
                    d[3] &= j;
                    d[5] &= j;
                    m['a'] &= j;
                    m['d'] &= j;
                    m['g'] &= j;
                break;

                case 6:
                    d[0] &= j;
                    d[6] &= j;
                    d[9] &= j;
                    m['a'] &= j;
                    m['b'] &= j;
                    m['f'] &= j;
                    m['g'] &= j;
                break;

                case 7:
                    d[8] = j;
                // PASSTHROUGH

                default:
                break;
            }

        while (!m.solved())
        {
            // 7 ^ 1 == a
            if (d[7] != 0 && d[1] != 0)
            {
                m['a'] &= d[7] ^ d[1];
                m['b'] &= ~m['a'];
                m['c'] &= ~m['a'];
                m['d'] &= ~m['a'];
                m['e'] &= ~m['a'];
                m['f'] &= ~m['a'];
                m['g'] &= ~m['a'];
            }

            // 4 ^ 1 == b|d
            if (d[4] != 0 && d[1] != 0)
            {
                m['b'] &= d[4] ^ d[1];
                m['d'] &= d[4] ^ d[1];
            }

            // prune segments that are known
            for (unsigned char i = 'a'; i != 'h'; ++i)
                if (m[i].count() == 1)
                    for (unsigned char j = 'a'; j != 'h'; ++j)
                        if (j != i)
                            m[j] &= ~m[i];
        }

        // kaput
        d =
        {
            m['a'] | m['b'] | m['c'] | m['e'] | m['f'] | m['g'],
            m['c'] | m['f'],
            m['a'] | m['c'] | m['d'] | m['e'] | m['g'],
            m['a'] | m['c'] | m['d'] | m['f'] | m['g'],
            m['b'] | m['c'] | m['d'] | m['f'],
            m['a'] | m['b'] | m['d'] | m['f'] | m['g'],
            m['a'] | m['b'] | m['d'] | m['e'] | m['f'] | m['g'],
            m['a'] | m['c'] | m['f'],
            0x7f,
            m['a'] | m['b'] | m['c'] | m['d'] | m['f'] | m['g']
        };

        res += (1000 * (std::find(d.cbegin(), d.cend(), i[DIGITS_OFF  ]) - d.cbegin()))
            +  ( 100 * (std::find(d.cbegin(), d.cend(), i[DIGITS_OFF+1]) - d.cbegin()))
            +  (  10 * (std::find(d.cbegin(), d.cend(), i[DIGITS_OFF+2]) - d.cbegin()))
            +  (       (std::find(d.cbegin(), d.cend(), i[DIGITS_OFF+3]) - d.cbegin()));
    }

    std::cout << res << std::endl;
}
