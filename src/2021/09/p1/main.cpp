// See COPYING.txt file for copyright and license details.

#include <array>
#include <iostream>
#include <cstdint>
#include <string>
#include <vector>

// constexpr size_t W = 10, L = 5;
constexpr size_t W = 100, L = 100;

using Height = uint8_t;
using Caves = std::array<std::array<Height,W>,L>;

Caves read_caves(std::istream& in)
{
    Caves res;

    for (Caves::value_type::size_type x = 0; x != L; ++x)
    {
        for (Caves::value_type::size_type y = 0; y != W; ++y)
        {
            char c;
            in >> c;
            res[x][y] = static_cast<Height>(c - '0');
        }

        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    return res;
}

int main()
{
    const Caves caves = read_caves(std::cin);
    std::vector<Height> low_points;

    for (Caves::value_type::size_type x = 0; x != L; ++x)
        for (Caves::value_type::size_type y = 0; y != W; ++y)
        {
            const Height& i = caves[x][y];
            if
            (
                (x == 0   || i < caves[x-1][y  ]) &&
                (y == 0   || i < caves[x  ][y-1]) &&
                (x == L-1 || i < caves[x+1][y  ]) &&
                (y == W-1 || i < caves[x  ][y+1])
            )
                low_points.push_back(i);
        }

    unsigned long long res = 0;
    for (const auto& i : low_points)
        res += i+1;

    std::cout << res << std::endl;
}
