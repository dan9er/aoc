// See COPYING.txt file for copyright and license details.

#include <array>
#include <iostream>
#include <cstdint>
#include <string>
#include <set>

// constexpr size_t W = 10, L = 5;
constexpr size_t W = 100, L = 100;

using Height = uint8_t;
using Caves = std::array<std::array<Height,W>,L>;
using Coords = std::pair<Caves::size_type,Caves::value_type::size_type>;

Caves read_caves(std::istream& in)
{
    Caves res;

    for (Caves::value_type::size_type x = 0; x != L; ++x)
    {
        for (Caves::value_type::size_type y = 0; y != W; ++y)
        {
            char c;
            in >> c;
            res[x][y] = static_cast<Height>(c - '0');
        }

        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    return res;
}

std::set<Coords> populate_basin(const Coords& coord, const Caves& caves)
{
    const Coords:: first_type& x = coord.first;
    const Coords::second_type& y = coord.second;

    std::set<Coords> res{coord}, i;

    // up
    if (x != 0 && caves[x-1][y  ] != 9 && caves[x][y] < caves[x-1][y  ])
    {
        i = populate_basin({x-1,y}, caves);
        res.insert(i.cbegin(), i.cend());
    }

    // left
    if (y != 0 && caves[x  ][y-1] != 9 && caves[x][y] < caves[x  ][y-1])
    {
        i = populate_basin({x,y-1}, caves);
        res.insert(i.cbegin(), i.cend());
    }

    // down
    if (x != L-1 && caves[x+1][y  ] != 9 && caves[x][y] < caves[x+1][y  ])
    {
        i = populate_basin({x+1,y}, caves);
        res.insert(i.cbegin(), i.cend());
    }

    // right
    if (y != W-1 && caves[x  ][y+1] != 9 && caves[x][y] < caves[x  ][y+1])
    {
        i = populate_basin({x,y+1}, caves);
        res.insert(i.cbegin(), i.cend());
    }

    return res;
}

int main()
{
    const Caves caves = read_caves(std::cin);
    std::set<Coords> low_points;

    struct SizeCmp
    {
        bool operator()(const std::set<Coords>& lhs, const std::set<Coords>& rhs) const
            {return lhs.size() > rhs.size();}
    };
    std::multiset<std::set<Coords>,SizeCmp> basins;

    for (Coords::first_type x = 0; x != L; ++x)
        for (Coords::second_type y = 0; y != W; ++y)
        {
            const Height& i = caves[x][y];
            if
            (
                (x == 0   || i < caves[x-1][y  ]) &&
                (y == 0   || i < caves[x  ][y-1]) &&
                (x == L-1 || i < caves[x+1][y  ]) &&
                (y == W-1 || i < caves[x  ][y+1])
            )
                low_points.insert({x,y});
        }

    for (auto& i : low_points)
        basins.insert(populate_basin(i, caves));

    // for (const auto& z : basins)
    // {
    //     for (const auto& w : z)
    //         std::cerr << w.first << ' ' << w.second << std::endl;
    //     std::cerr << std::endl;
    // }

    unsigned long long res = 1;
    auto end = basins.cbegin();
    ++end; ++end; ++end;
    for (auto i = basins.cbegin(); i != end; ++i)
        // std::cerr << i->size() << std::endl;
        res *= i->size();

    std::cout << res << std::endl;
}
