// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <cstdint>
#include <string>
#include <map>
#include <stack>

enum chunk_type : char
{
    normal = '(',
    square = '[',
    curl = '{',
    angle = '<'
};

const std::map<char,chunk_type> chunk_end
{
    {')', chunk_type::normal},
    {']', chunk_type::square},
    {'}', chunk_type::curl},
    {'>', chunk_type::angle}
};

unsigned err_score(const std::string& line)
{
    static const std::map<chunk_type,unsigned> score
    {
        {chunk_type::normal, 3},
        {chunk_type::square, 57},
        {chunk_type::curl, 1197},
        {chunk_type::angle, 25137}
    };

    std::stack<chunk_type> chunks;

    for (const char& c : line)
    {
        switch (c)
        {
            case '(':
            case '[':
            case '{':
            case '<':
                chunks.push(static_cast<chunk_type>(c));
            break;

            case ')':
            case ']':
            case '}':
            case '>':
                if (chunks.top() == chunk_end.at(c))
                    chunks.pop();
                else
                    return score.at(chunk_end.at(c));
            break;

            default:
                return 0;
        }
    }

    return 0;
}

int main()
{
    unsigned long long res = 0;

    std::string line;
    while (std::cin >> line)
    {
        res += err_score(line);
    }

    std::cout << res << std::endl;
}
