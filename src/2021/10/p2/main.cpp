// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <cstdint>
#include <set>
#include <string>
#include <map>
#include <stack>

enum chunk_type : char
{
    normal = '(',
    square = '[',
    curl = '{',
    angle = '<'
};

const std::map<char,chunk_type> end_to_chunk
{
    {')', chunk_type::normal},
    {']', chunk_type::square},
    {'}', chunk_type::curl},
    {'>', chunk_type::angle}
};

const std::map<chunk_type,char> chunk_to_end
{
    {chunk_type::normal, ')'},
    {chunk_type::square, ']'},
    {chunk_type::curl, '}'},
    {chunk_type::angle, '>'}
};

bool is_corrupt(const std::string& line)
{
    std::stack<chunk_type> chunks;

    for (const char& c : line)
    {
        switch (c)
        {
            case '(':
            case '[':
            case '{':
            case '<':
                chunks.push(static_cast<chunk_type>(c));
            break;

            case ')':
            case ']':
            case '}':
            case '>':
                if (chunks.top() == end_to_chunk.at(c))
                    chunks.pop();
                else
                    return true;
            break;

            default:
                return false;
        }
    }

    return false;
}

unsigned long long complete(const std::string& line)
{
    static const std::map<chunk_type,unsigned> score
    {
        {chunk_type::normal, 1},
        {chunk_type::square, 2},
        {chunk_type::curl, 3},
        {chunk_type::angle, 4}
    };

    std::stack<chunk_type> chunks;

    for (const char& c : line)
    {
        switch (c)
        {
            case '(':
            case '[':
            case '{':
            case '<':
                chunks.push(static_cast<chunk_type>(c));
            break;

            case ')':
            case ']':
            case '}':
            case '>':
                chunks.pop();
        }
    }

    unsigned long long res = 0;

    for (chunk_type i = chunks.top(); true;)
    {
        res *= 5;
        res += score.at(i);

        chunks.pop();

        if (chunks.empty())
            break;
        else
            i = chunks.top();
    }

    return res;
}

int main()
{
    std::multiset<unsigned long long> scores;

    std::string line;
    while (std::cin >> line)
    {
        if (is_corrupt(line))
            continue;

        scores.insert(complete(line));
    }

    auto res = scores.cbegin();
    for (size_t i = 0; i != scores.size()/2; ++i)
        ++res;

    std::cout << *res << std::endl;
}
