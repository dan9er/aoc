// See COPYING.txt file for copyright and license details.

#include <algorithm>
#include <array>
#include <iostream>
#include <cstdint>

using Octopus = uint16_t;
using Cavern = std::array<std::array<Octopus,10>,10>;

Cavern read_cavern(std::istream& in)
{
    Cavern res;

    for (Cavern::size_type x = 0; x != 10; ++x)
    {
        for (Cavern::value_type::size_type y = 0; y != 10; ++y)
        {
            char c;
            in >> c;
            res[x][y] = static_cast<Octopus>(c - '0');
        }

        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    return res;
}

uint64_t flash(Cavern& c, const Cavern::size_type& x, const Cavern::size_type& y)
{
    uint64_t res = 1;
    c[x][y] = 0;

    if (x != 0 && c[x-1][y])
    {
        ++c[x-1][y];
        if (c[x-1][y] > 9)
            res += flash(c, x-1, y);
    }

    if (x != 9 && c[x+1][y])
    {
        ++c[x+1][y];
        if (c[x+1][y] > 9)
            res += flash(c, x+1, y);
    }

    if (y != 0 && c[x][y-1])
    {
        ++c[x][y-1];
        if (c[x][y-1] > 9)
            res += flash(c, x, y-1);
    }

    if (y != 9 && c[x][y+1])
    {
        ++c[x][y+1];
        if (c[x][y+1] > 9)
            res += flash(c, x, y+1);
    }

    if (x != 0 && y != 0 && c[x-1][y-1])
    {
        ++c[x-1][y-1];
        if (c[x-1][y-1] > 9)
            res += flash(c, x-1, y-1);
    }

    if (x != 9 && y != 9 && c[x+1][y+1])
    {
        ++c[x+1][y+1];
        if (c[x+1][y+1] > 9)
            res += flash(c, x+1, y+1);
    }

    if (x != 0 && y != 9 && c[x-1][y+1])
    {
        ++c[x-1][y+1];
        if (c[x-1][y+1] > 9)
            res += flash(c, x-1, y+1);
    }

    if (x != 9 && y != 0 && c[x+1][y-1])
    {
        ++c[x+1][y-1];
        if (c[x+1][y-1] > 9)
            res += flash(c, x+1, y-1);
    }

    return res;
}

int main()
{
    Cavern cavern = read_cavern(std::cin);
    uint64_t res = 0;

    for (uint16_t s = 0; s != 100; ++s)
    {
        for (Cavern::size_type x = 0; x != 10; ++x)
            for (Cavern::value_type::size_type y = 0; y != 10; ++y)
                ++cavern[x][y];

        for (Cavern::size_type x = 0; x != 10; ++x)
            for (Cavern::value_type::size_type y = 0; y != 10; ++y)
                if (cavern[x][y] > 9)
                    res += flash(cavern, x, y);

        for (const auto& x : cavern)
        {
            for (const auto& y : x)
                std::cerr << y;
            std::cerr << std::endl;
        }
        std::cerr << std::endl;
    }

    std::cout << res << std::endl;
}
