// See COPYING.txt file for copyright and license details.

#include <algorithm>
#include <iostream>
#include <set>
#include <memory>
#include <vector>
#include <string>
#include <cctype>
#include <cassert>

inline
bool isupper(const char& c)
    {return std::isupper(static_cast<unsigned char>(c));}

inline
char tolower(const char& c)
    {return static_cast<char>(std::tolower(static_cast<unsigned char>(c)));}

std::string tolower(std::string s)
{
    for (char& c : s)
        c = tolower(c);
    return s;
}

struct Cave
{
    std::string name;
    bool big;
    std::set<std::shared_ptr<Cave>> nb;

    Cave(const std::string& n)
    {
        name = tolower(n);
        big = static_cast<unsigned char>(std::isupper(n[0]));
    }

    void connect(std::shared_ptr<Cave>& n)
        {nb.insert(n);}
};

bool operator<(const Cave& lhs, const Cave& rhs)
{
    if (lhs.name == rhs.name)
        return lhs.big < rhs.big;
    else
        return lhs.name < rhs.name;
}

bool operator<(const std::shared_ptr<Cave>& lhs, const std::shared_ptr<Cave>& rhs)
{
    if (!lhs) // lhs or both are nullptr
        return false;
    else if (!rhs) // only rhs is nullptr
        return true;
    else // both valid
        return *lhs < *rhs;

    // NEVER REACHED
    assert(false);
}

struct System
{
    std::set<std::shared_ptr<Cave>> data;

    std::shared_ptr<const Cave> get_start() const
    {
        return *std::find_if(data.cbegin(), data.cend(), [](const std::shared_ptr<const Cave>& c)
            {return (c->name == "start" && !c->big);});
    }

    void insert(const std::string& lhs, const std::string& rhs)
    {
        std::shared_ptr<Cave> l = *(data.emplace(new Cave(lhs)).first),
                              r = *(data.emplace(new Cave(rhs)).first);

        l->connect(r);
        if (!(lhs == "start" || rhs == "end"))
            r->connect(l);
    }

    System(std::istream& in)
    {
        while (in)
        {
            std::string i, j;
            std::getline(in, i, '-');

            if (i.empty())
                break;

            std::getline(in, j);

            if (i == "end" || j == "start")
                insert(j, i);
            else
                insert(i, j);
        }
    }
};

struct Path
{
    std::vector<std::shared_ptr<const Cave>> stack;

    unsigned operator()(const std::shared_ptr<const Cave>& node)
    {
        unsigned res = 0;

        // push node to stack
        stack.push_back(node);

        // go through neighbours
        for (const std::shared_ptr<Cave>& i : node->nb)
        {
            // check if about to visit small cave twice
            if
            (
                !i->big
                &&
                std::find(stack.cbegin(), stack.cend(), i) != stack.cend()
            )
                continue; // skip it

            // check if about to visit end
            if (i->name == "end" && !i->big)
            {
                // increment res and skip
                ++res;
                continue;
            }

            // recurse
            res += (*this)(i);
        }

        // pop and return
        stack.pop_back();
        return res;
    }
};

int main()
{
    System sys(std::cin);

    std::cout << Path()(sys.get_start()) << std::endl;
}
