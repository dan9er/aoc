// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <cstdint>
#include <string>
#include <type_traits>
#include <vector>

struct Fold
{
    enum Axis : bool
    {
        x = false,
        y = true
    };

    Axis axis : 1;
    uint16_t n : 15;

    Fold(const Axis& a, const uint16_t& n) :
        axis(a), n(n)
    {};
};

struct Paper
{
    std::vector<std::vector<bool>> data;

    Paper(const size_t& x, const size_t& y) :
        data(y,std::vector<bool>(x,false))
    {}

    std::vector<bool>::reference at(const size_t& x, const size_t& y)
        {return data[y][x];}

    Paper(std::istream& in, const size_t& w, const size_t& l) :
        Paper(w,l)
    {
        while (in)
        {
            if (in.peek() == '\n')
                break;

            std::string i, j;
            std::getline(in, i, ',');
            std::getline(in, j);

            at(std::stoull(i), std::stoull(j)) = true;
        }

        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    void fold_y(const size_t& n)
    {
        auto i = data.begin();
        auto o = data.crbegin();
        for (; i != data.begin() + n; ++i, ++o)
        {
            auto j = i->begin();
            auto u = o->cbegin();
            for (; j != i->end(); ++j, ++u)
                *j = *j | *u;
        }

        data.erase(i, data.end());
    }

    void fold_x(const size_t& n)
    {
        for (auto i = data.begin(); i != data.end(); ++i)
        {
            auto j = i->begin();
            auto u = i->crbegin();
            for (; j != i->begin() + n; ++j, ++u)
                *j = *j | *u;

            i->erase(j, i->end());
        }
    }

    unsigned long long count() const
    {
        unsigned long long res = 0;
        for (const auto& y : data)
            for (const auto& x : y)
                if (x)
                    ++res;
        return res;
    }
};

std::vector<Fold> read_folds(std::istream& in)
{
    std::vector<Fold> res;

    while (in)
    {
        // skip "fold along "
        in.seekg(11, std::ios_base::cur);

        char a;
        in.get(a);

        in.seekg(1, std::ios_base::cur);

        std::string n;
        std::getline(in, n);

        if (n.empty())
            break;

        switch (a)
        {
            case 'x':
                res.emplace_back(Fold::x, std::stoi(n));
            break;

            case 'y':
                res.emplace_back(Fold::y, std::stoi(n));
        }
    }

    return res;
}

// constexpr size_t X = 11, Y = 15;
constexpr size_t X = 1311, Y = 895;

int main()
{
    Paper paper(std::cin,X,Y);
    const std::vector<Fold> folds = read_folds(std::cin);

    if (folds[0].axis == Fold::y)
        paper.fold_y(folds[0].n);
    else // x
        paper.fold_x(folds[0].n);

    std::cout << paper.count() << std::endl;
}
