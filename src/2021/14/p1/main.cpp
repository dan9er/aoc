// See COPYING.txt file for copyright and license details.

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <type_traits>

struct Rules
{
    using internal_t = std::map<std::string, char>;
    internal_t data = {};

    Rules(std::istream& in)
    {
        while (in)
        {
            std::string k;

            std::getline(in, k, ' ');
            if (k.empty())
                break;

            in.seekg(3, std::ios_base::cur);

            char r;
            in.get(r);

            data[std::move(k)] = std::move(r);

            in.seekg(2, std::ios_base::cur);
        }
    }

    const internal_t::mapped_type& at(const internal_t::key_type& n) const
        {return data.at(n);}
};

struct Polymer
{
    using internal_t = std::string;
    internal_t data = "";

    Polymer(std::istream& in)
    {
        in >> data;
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    void expand(const Rules& rules)
    {
        for (auto i = data.begin(); i != data.end()-1; ++i)
        {
            internal_t v{*i, *(i+1)};
            i = data.insert(i+1, rules.at(v));
        }
    }

    unsigned long long score()
    {
        std::map<char,unsigned long long> count;
        for (const char& i : data)
            ++count[i];

        auto ret = std::minmax_element
            (
                count.cbegin(),
                count.cend(),
                [](
                    const std::map<char,unsigned long long>::value_type& lhs,
                    const std::map<char,unsigned long long>::value_type& rhs
                ){
                    return lhs.second < rhs.second;
                }
            );

        return ret.second->second - ret.first->second;
    }
};

int main()
{
    Polymer poly(std::cin);
    Rules rules(std::cin);

    for (uint8_t i = 0; i != 10; ++i)
        poly.expand(rules);

    std::cout << poly.score() << std::endl;
}
