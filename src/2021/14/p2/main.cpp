// See COPYING.txt file for copyright and license details.

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <cstdint>
#include <type_traits>

struct Rules
{
    using internal_t = std::map<std::string, std::pair<std::string,std::string>>;
    internal_t data = {};

    Rules(std::istream& in)
    {
        while (in)
        {
            std::string k;

            std::getline(in, k, ' ');
            if (k.empty())
                break;

            in.seekg(3, std::ios_base::cur);

            char r;
            in.get(r);

            data[std::move(k)] = {{k[0],r},{r,k[1]}};

            in.seekg(2, std::ios_base::cur);
        }
    }

    const internal_t::mapped_type& at(const internal_t::key_type& n) const
        {return data.at(n);}
};

struct Polymer
{
    using internal_t = std::map<std::string, uint64_t>;
    internal_t pairs = {};
    std::map<char, uint64_t> letters = {};

    template <typename T, typename U, typename V>
    void inc_helper(std::map<T, U>& map, const T& key, const V& delta, const U& def = 0)
    {
        if (!map.count(key))
            map[key] = def;
        map[key] += delta;
    }
    template <typename T, typename U, typename V>
    void dec_helper(std::map<T, U>& map, const T& key, const V& delta, const U& def = 0)
    {
        if (!map.count(key))
            map[key] = def;
        map[key] -= delta;
    }

    Polymer(std::istream& in)
    {
        std::string str;
        in >> str;

        for (auto i = str.begin(); i != str.end()-1; ++i)
        {
            inc_helper(this->pairs, {*i, *(i+1)}, 1uLL);
            inc_helper(this->letters, *i, 1uLL);
        }
        inc_helper(this->letters, *(str.end()-1), 1uLL);

        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    void expand(const Rules& rules)
    {
        std::map<std::string, int64_t> delta = {};

        for (auto i : this->pairs)
        {
            const Rules::internal_t::mapped_type& match = rules.at(i.first);

            // remove old pair
            dec_helper(delta, i.first, i.second);

            // add new pairs
            inc_helper(delta, match.first , i.second);
            inc_helper(delta, match.second, i.second);

            // add new letter
            inc_helper(this->letters, match.first[1], i.second);
        }

        // apply delta
        for (auto i : delta)
            inc_helper(this->pairs, i.first, i.second);
    }

    unsigned long long score()
    {
        auto ret = std::minmax_element
            (
                this->letters.cbegin(),
                this->letters.cend(),
                [](
                    const std::map<char,uint64_t>::value_type& lhs,
                    const std::map<char,uint64_t>::value_type& rhs
                ){
                    return lhs.second < rhs.second;
                }
            );

        return ret.second->second - ret.first->second;
    }
};

int main()
{
    Polymer poly(std::cin);
    Rules rules(std::cin);

    for (uint8_t i = 0; i != 40; ++i)
        poly.expand(rules);

    std::cout << poly.score() << std::endl;
}
