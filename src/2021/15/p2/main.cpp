// See COPYING.txt file for copyright and license details.

#include <algorithm>
#include <iostream>
#include <cstdint>
#include <memory>
#include <set>
#include <utility>
#include <array>
#include <vector>

using Coord = std::pair<size_t, size_t>;

Coord operator+(Coord lhs, const Coord& rhs)
{
    lhs.first  += rhs.first;
    lhs.second += rhs.second;
    return lhs;
}

struct Node
{
    static constexpr uint16_t INF = UINT16_MAX;

    bool     visited :  1;
    uint8_t  cost    :  7;
    uint16_t dist    : 16;

    Node(const char& c = '0') :
        visited(false),
        cost(static_cast<uint8_t>(c - '0')),
        dist(INF)
    {};
};

template <size_t X, size_t Y>
struct Cavern
{
    std::unique_ptr<std::array<std::array<Node,X>,Y>> data;

    typename std::array<Node,X>::reference at(const Coord& n)
        {return (*this->data)[n.second][n.first];}

    Cavern()
    {
        this->data.reset(new std::array<std::array<Node,X>,Y>);
        std::array<Node,X> row;
        std::fill_n(data->begin(), Y, row);
    }

    Cavern(std::istream& in)
    {
        this->data.reset(new std::array<std::array<Node,X>,Y>);

        for (size_t y = 0; y != Y; ++y)
        {
            for (size_t x = 0; x != X; ++x)
            {
                char c;
                in.get(c);
                this->at({x,y}) = c;
            }

            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    };

    Cavern<X*5,Y*5> expand()
    {
        Cavern<X*5,Y*5> res;

        for (uint8_t rep_y = 0; rep_y != 5; ++rep_y)
            for (uint8_t rep_x = 0; rep_x != 5; ++rep_x)
                for (size_t y = 0; y != Y; ++y)
                    for (size_t x = 0; x != X; ++x)
                    {
                        Node ref = this->at({x,y});
                        ref.cost += rep_x + rep_y;

                        // wrap 9 to 1
                        if (ref.cost > 9)
                            ref.cost -= 9;

                        res.at({(rep_x*X) + x, (rep_y*Y) + y}) = std::move(ref);
                    }

        return res;
    }

    std::vector<Coord> node_neighbours(const Coord& c)
    {
        const Coord::first_type & x = c.first;
        const Coord::second_type& y = c.second;

        std::vector<Coord> res;

        if (x != 0   && !this->at({c.first-1, c.second}).visited)
            res.push_back({c.first-1, c.second});

        if (x != X-1 && !this->at({c.first+1, c.second}).visited)
            res.push_back({c.first+1, c.second});

        if (y != 0   && !this->at({c.first, c.second-1}).visited)
            res.push_back({c.first, c.second-1});

        if (y != Y-1 && !this->at({c.first, c.second+1}).visited)
            res.push_back({c.first, c.second+1});

        return res;
    }

    uint16_t dijkstra()
    {
        // current node
        Coord current = {0,0};

        // set of unvisited, non-INF nodes
        std::set<Coord> populated = {{0,0}};

        // start node distance is 0
        this->at({0,0}).dist = 0;

        while (true)
        {
            // go over unvisited neighbours
            for (const auto& n : node_neighbours(current))
            {
                // calculate total distance after moving to neighbour node
                const uint16_t move = this->at(current).dist + this->at(n).cost;

                // set neighbour's distance if smaller
                if (this->at(n).dist > move)
                {
                    this->at(n).dist = move;
                    populated.insert(n);
                }
            }

            // mark current node visited
            this->at(current).visited = true;
            populated.erase(current);

            // if current node was end node, return distance
            if (current == Coord{X-1,Y-1})
                return this->at(current).dist;

            // set lowest cost unvisited node as current
            current = *std::min_element(populated.cbegin(), populated.cend(), [this](const Coord& lhs, const Coord& rhs)
                {return this->at(lhs).dist < this->at(rhs).dist;});
        }
    }
};

int main()
{
    Cavern<100,100> cav(std::cin);
    std::cout << cav.expand().dijkstra() << std::endl;
}
