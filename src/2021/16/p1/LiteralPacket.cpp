// See COPYING.txt file for copyright and license details.

#include "LiteralPacket.hpp"

#include <cstdint>

#include "BitsStream.hpp"
#include "PacketType.hpp"
#include "Packet_base.hpp"

LiteralPacket::LiteralPacket(const uint8_t& ver, BitsStream& in) :
        Packet_base(ver, PacketType::literal),
        value(0)
{
    while (true)
    {
        const bool cont = in.read<bool>(1);
        uint8_t group = in.read<uint8_t>(4);

        this->value <<= 4;
        this->value |= group;

        if (!cont)
            break;
    }
}

/*virtual*/ uint64_t LiteralPacket::ver_sum() const
    {return this->version;}
