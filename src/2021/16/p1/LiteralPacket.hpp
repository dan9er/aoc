// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>

#include "BitsStream.hpp"
#include "Packet_base.hpp"

struct LiteralPacket : public Packet_base
{
    uint64_t value;

    LiteralPacket(const uint8_t&, BitsStream&);

    virtual ~LiteralPacket()
        = default;

    virtual uint64_t ver_sum() const;
};
