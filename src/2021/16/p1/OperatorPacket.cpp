// See COPYING.txt file for copyright and license details.

#include "OperatorPacket.hpp"

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "PacketType.hpp"
#include "Packet_base.hpp"
#include "Packet.hpp"

OperatorPacket::OperatorPacket(const uint8_t& ver, const PacketType& t, BitsStream& in) :
    Packet_base(ver, t)
{
    if (in.read<bool>(1)) // no. of children
    {
        const uint16_t len = in.read<uint16_t>(11);

        for (uint16_t i = 0; i != len; ++i)
            this->children.emplace_back(in);
    }
    else // no. of bits
    {
        const uint16_t len = in.read<uint16_t>(15);
        const BitsStream::const_iterator beg = in.g;

        while (in.g != beg + len)
            this->children.emplace_back(in);
    }
}

/*virtual*/ uint64_t OperatorPacket::ver_sum() const
{
    uint64_t res = this->version;
    for (const Packet& i : children)
        res += i.ver_sum();
    return res;
}
