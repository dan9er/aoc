// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "PacketType.hpp"
#include "Packet_base.hpp"
#include "Packet.hpp"

struct OperatorPacket : public Packet_base
{
    std::vector<Packet> children;

    OperatorPacket(const uint8_t&, const PacketType&, BitsStream&);

    virtual ~OperatorPacket()
        = default;

    virtual uint64_t ver_sum() const;
};
