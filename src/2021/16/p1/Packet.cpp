// See COPYING.txt file for copyright and license details.

#include "Packet.hpp"

#include <memory>

#include "BitsStream.hpp"
#include "PacketType.hpp"
#include "Packet_base.hpp"
#include "LiteralPacket.hpp"
#include "OperatorPacket.hpp"

Packet::Packet(BitsStream& in)
{
    const uint8_t ver = in.read<uint8_t>(3);
    const PacketType t = static_cast<PacketType>(in.read<uint8_t>(3));

    switch (t)
    {
        case PacketType::literal:
            this->data = std::unique_ptr<Packet_base>(new LiteralPacket(ver, in));
        break;

        default:
            this->data = std::unique_ptr<Packet_base>(new OperatorPacket(ver, t, in));
    }
}

uint64_t Packet::ver_sum() const
    {return data->ver_sum();}
