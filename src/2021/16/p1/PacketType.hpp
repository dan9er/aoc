// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>

enum class PacketType : uint8_t
{
    operator_0,
    operator_1,
    operator_2,
    operator_3,
    literal,
    operator_5,
    operator_6,
    operator_7
};
