// See COPYING.txt file for copyright and license details.

#include "Packet_base.hpp"

#include <cstdint>
#include <type_traits>

#include "PacketType.hpp"

Packet_base::Packet_base(const uint8_t& ver, const PacketType& t) :
    version(ver),
    type   (t)
{}
Packet_base::Packet_base(const uint8_t& ver, PacketType&& t) :
    version(ver),
    type   (std::move(t))
{}
