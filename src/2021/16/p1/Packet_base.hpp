// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>

#include "PacketType.hpp"

struct Packet_base
{
       uint8_t version : 3;
    PacketType type    : 3;

    Packet_base(const uint8_t&, const PacketType &);
    Packet_base(const uint8_t&,       PacketType&&);

    virtual ~Packet_base()
        = default;

    virtual uint64_t ver_sum() const
        = 0; // pure
};
