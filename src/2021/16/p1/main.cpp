// See COPYING.txt file for copyright and license details.

#include <bitset>
#include <iostream>
#include <cstdint>
#include <cstddef>
#include <istream>
#include <vector>

#include "BitsStream.hpp"
#include "PacketType.hpp"
#include "Packet.hpp"

int main()
{
    BitsStream bits(std::cin);

    std::cout << Packet(bits).ver_sum() << std::endl;
}
