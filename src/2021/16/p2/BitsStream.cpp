// See COPYING.txt file for copyright and license details.

#include "BitsStream.hpp"

#include <vector>
#include <istream>

BitsStream::BitsStream(std::istream& in)
{
    char c;
    while (in.get(c))
        switch (c)
        {
            case '0':
                this->data.insert(this->data.end(), {0,0,0,0});
            break;
            case '1':
                this->data.insert(this->data.end(), {0,0,0,1});
            break;
            case '2':
                this->data.insert(this->data.end(), {0,0,1,0});
            break;
            case '3':
                this->data.insert(this->data.end(), {0,0,1,1});
            break;
            case '4':
                this->data.insert(this->data.end(), {0,1,0,0});
            break;
            case '5':
                this->data.insert(this->data.end(), {0,1,0,1});
            break;
            case '6':
                this->data.insert(this->data.end(), {0,1,1,0});
            break;
            case '7':
                this->data.insert(this->data.end(), {0,1,1,1});
            break;
            case '8':
                this->data.insert(this->data.end(), {1,0,0,0});
            break;
            case '9':
                this->data.insert(this->data.end(), {1,0,0,1});
            break;
            case 'A':
                this->data.insert(this->data.end(), {1,0,1,0});
            break;
            case 'B':
                this->data.insert(this->data.end(), {1,0,1,1});
            break;
            case 'C':
                this->data.insert(this->data.end(), {1,1,0,0});
            break;
            case 'D':
                this->data.insert(this->data.end(), {1,1,0,1});
            break;
            case 'E':
                this->data.insert(this->data.end(), {1,1,1,0});
            break;
            case 'F':
                this->data.insert(this->data.end(), {1,1,1,1});
            break;

            case '\n': ;
        }

    this->g = this->data.begin();
}
