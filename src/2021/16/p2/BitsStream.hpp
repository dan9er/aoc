// See COPYING.txt file for copyright and license details.

#pragma once

#include <vector>
#include <istream>

struct BitsStream
{
    using          internal_type = std::vector<bool>;
    using              size_type = internal_type::     size_type;
    using               iterator = internal_type::      iterator;
    using         const_iterator = internal_type::const_iterator;

    internal_type data;
         iterator g;

    BitsStream(std::istream&);

    template <typename T>
    T read(const size_type& bits)
    {
        T res = 0;
        const const_iterator end = this->g + bits;

        for (; this->g != end; ++this->g)
        {
            res <<= 1;
            if (*this->g)
                res |= 1;
        }

        return res;
    }
};
