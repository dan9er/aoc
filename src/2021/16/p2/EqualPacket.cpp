// See COPYING.txt file for copyright and license details.

#include "EqualPacket.hpp"

#include <algorithm>
#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"
#include "Packet.hpp"

EqualPacket::EqualPacket(const uint8_t& ver, BitsStream& in) :
    OperatorPacket(ver, in)
{}

/*virtual*/ uint64_t EqualPacket::eval() const
    {return this->children[0].eval() == this->children[1].eval() ? 1 : 0;}
