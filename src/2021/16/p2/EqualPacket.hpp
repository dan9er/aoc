// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"

struct EqualPacket : public OperatorPacket
{
    EqualPacket(const uint8_t&, BitsStream&);

    virtual ~EqualPacket()
        = default;

    virtual uint64_t eval() const;
};
