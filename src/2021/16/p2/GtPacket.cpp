// See COPYING.txt file for copyright and license details.

#include "GtPacket.hpp"

#include <algorithm>
#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"
#include "Packet.hpp"

GtPacket::GtPacket(const uint8_t& ver, BitsStream& in) :
    OperatorPacket(ver, in)
{}

/*virtual*/ uint64_t GtPacket::eval() const
    {return this->children[0].eval() > this->children[1].eval() ? 1 : 0;}
