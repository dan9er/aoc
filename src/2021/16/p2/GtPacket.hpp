// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"

struct GtPacket : public OperatorPacket
{
    GtPacket(const uint8_t&, BitsStream&);

    virtual ~GtPacket()
        = default;

    virtual uint64_t eval() const;
};
