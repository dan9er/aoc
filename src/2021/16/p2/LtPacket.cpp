// See COPYING.txt file for copyright and license details.

#include "LtPacket.hpp"

#include <algorithm>
#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"
#include "Packet.hpp"

LtPacket::LtPacket(const uint8_t& ver, BitsStream& in) :
    OperatorPacket(ver, in)
{}

/*virtual*/ uint64_t LtPacket::eval() const
    {return this->children[0].eval() < this->children[1].eval() ? 1 : 0;}
