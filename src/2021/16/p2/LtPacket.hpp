// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"

struct LtPacket : public OperatorPacket
{
    LtPacket(const uint8_t&, BitsStream&);

    virtual ~LtPacket()
        = default;

    virtual uint64_t eval() const;
};
