// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"

struct MaxPacket : public OperatorPacket
{
    MaxPacket(const uint8_t&, BitsStream&);

    virtual ~MaxPacket()
        = default;

    virtual uint64_t eval() const;
};
