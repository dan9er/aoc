// See COPYING.txt file for copyright and license details.

#include "MinPacket.hpp"

#include <algorithm>
#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"
#include "Packet.hpp"

MinPacket::MinPacket(const uint8_t& ver, BitsStream& in) :
    OperatorPacket(ver, in)
{}

/*virtual*/ uint64_t MinPacket::eval() const
{
    return std::min_element(this->children.cbegin(), this->children.cend(), [](const Packet& lhs, const Packet& rhs)
        {return lhs.eval() < rhs.eval();})->eval();
}
