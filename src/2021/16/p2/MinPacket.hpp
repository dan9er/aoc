// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"

struct MinPacket : public OperatorPacket
{
    MinPacket(const uint8_t&, BitsStream&);

    virtual ~MinPacket()
        = default;

    virtual uint64_t eval() const;
};
