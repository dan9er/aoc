// See COPYING.txt file for copyright and license details.

#include "OperatorPacket.hpp"

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "Packet_base.hpp"
#include "Packet.hpp"

OperatorPacket::OperatorPacket(const uint8_t& ver, BitsStream& in) :
    Packet_base(ver)
{
    if (in.read<bool>(1)) // no. of children
    {
        const uint16_t len = in.read<uint16_t>(11);

        for (uint16_t i = 0; i != len; ++i)
            this->children.emplace_back(in);
    }
    else // no. of bits
    {
        const uint16_t len = in.read<uint16_t>(15);
        const BitsStream::const_iterator end = in.g + len;

        while (in.g != end)
            this->children.emplace_back(in);
    }
}
