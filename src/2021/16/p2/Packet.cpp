// See COPYING.txt file for copyright and license details.

#include "Packet.hpp"

#include <cstdint>
#include <memory>

#include "BitsStream.hpp"
#include "Packet_base.hpp"
#include "SumPacket.hpp"
#include "ProductPacket.hpp"
#include "MinPacket.hpp"
#include "MaxPacket.hpp"
#include "LiteralPacket.hpp"
#include "GtPacket.hpp"
#include "LtPacket.hpp"
#include "EqualPacket.hpp"

Packet::Packet(BitsStream& in)
{
    enum class PacketType : uint8_t
    {
        sum,
        product,
        min,
        max,
        literal,
        gt,
        lt,
        equal
    };

    const uint8_t ver = in.read<uint8_t>(3);
    const PacketType t = static_cast<PacketType>(in.read<uint8_t>(3));

    switch (t)
    {
        case PacketType::sum:
            this->data = std::unique_ptr<Packet_base>(new SumPacket    (ver, in));
        break;

        case PacketType::product:
            this->data = std::unique_ptr<Packet_base>(new ProductPacket(ver, in));
        break;

        case PacketType::min:
            this->data = std::unique_ptr<Packet_base>(new MinPacket    (ver, in));
        break;

        case PacketType::max:
            this->data = std::unique_ptr<Packet_base>(new MaxPacket    (ver, in));
        break;

        case PacketType::literal:
            this->data = std::unique_ptr<Packet_base>(new LiteralPacket(ver, in));
        break;

        case PacketType::gt:
            this->data = std::unique_ptr<Packet_base>(new GtPacket     (ver, in));
        break;

        case PacketType::lt:
            this->data = std::unique_ptr<Packet_base>(new LtPacket     (ver, in));
        break;

        case PacketType::equal:
            this->data = std::unique_ptr<Packet_base>(new EqualPacket  (ver, in));
        break;
    }
}

uint64_t Packet::eval() const
    {return data->eval();}
