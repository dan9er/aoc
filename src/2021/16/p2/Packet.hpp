// See COPYING.txt file for copyright and license details.

#pragma once

#include <memory>

#include "BitsStream.hpp"
#include "Packet_base.hpp"

struct Packet
{
    std::unique_ptr<Packet_base> data;

    Packet(BitsStream&);

    uint64_t eval() const;
};
