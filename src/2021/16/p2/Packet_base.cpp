// See COPYING.txt file for copyright and license details.

#include "Packet_base.hpp"

#include <cstdint>

Packet_base::Packet_base(const uint8_t& ver) :
    version(ver)
{}
