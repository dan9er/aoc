// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>

struct Packet_base
{
    uint8_t version : 3;

    Packet_base(const uint8_t&);

    virtual ~Packet_base()
        = default;

    virtual uint64_t eval() const
        = 0; // pure
};
