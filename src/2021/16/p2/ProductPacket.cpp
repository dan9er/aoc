// See COPYING.txt file for copyright and license details.

#include "ProductPacket.hpp"

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"
#include "Packet.hpp"

ProductPacket::ProductPacket(const uint8_t& ver, BitsStream& in) :
    OperatorPacket(ver, in)
{}

/*virtual*/ uint64_t ProductPacket::eval() const
{
    uint64_t res = 1;
    for (const Packet& i : this->children)
        res *= i.eval();
    return res;
}
