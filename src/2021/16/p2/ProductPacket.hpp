// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"

struct ProductPacket : public OperatorPacket
{
    ProductPacket(const uint8_t&, BitsStream&);

    virtual ~ProductPacket()
        = default;

    virtual uint64_t eval() const;
};
