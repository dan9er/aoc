// See COPYING.txt file for copyright and license details.

#include "SumPacket.hpp"

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"
#include "Packet.hpp"

SumPacket::SumPacket(const uint8_t& ver, BitsStream& in) :
    OperatorPacket(ver, in)
{}

/*virtual*/ uint64_t SumPacket::eval() const
{
    uint64_t res = 0;
    for (const Packet& i : this->children)
        res += i.eval();
    return res;
}
