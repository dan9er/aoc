// See COPYING.txt file for copyright and license details.

#pragma once

#include <cstdint>
#include <vector>

#include "BitsStream.hpp"
#include "OperatorPacket.hpp"

struct SumPacket : public OperatorPacket
{
    SumPacket(const uint8_t&, BitsStream&);

    virtual ~SumPacket()
        = default;

    virtual uint64_t eval() const;
};
