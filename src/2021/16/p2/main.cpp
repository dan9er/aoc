// See COPYING.txt file for copyright and license details.

#include <iostream>

#include "BitsStream.hpp"
#include "Packet.hpp"

int main()
{
    BitsStream bits(std::cin);

    std::cout << Packet(bits).eval() << std::endl;
}
