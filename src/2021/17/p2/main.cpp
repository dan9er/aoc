// See COPYING.txt file for copyright and license details.

#include <iostream>
#include <cstdint>
#include <utility>

using X_t  = uint32_t;
using Y_t  =  int32_t;
using Xv_t =  int32_t;
using Yv_t =  int32_t;

using Coord = std::pair<X_t , Y_t>;
using Vel   = std::pair<Xv_t, Yv_t>;
using Range = std::pair<std::pair<X_t, X_t>, std::pair<Y_t, Y_t>>;

enum class FireResult
{
    hit,
    over,
    under
};

FireResult fire(Vel vel, const Range& target)
{
    Coord pos = {0,0};

    while (true)
    {
        pos.first  += vel.first;
        pos.second += vel.second;

        if (vel.first)
            --vel.first;

        --vel.second;

        // hit
        if
        (
               target.first .first <= pos.first  && pos.first  <= target.first .second
            && target.second.first <= pos.second && pos.second <= target.second.second
        )
            return FireResult::hit;

        // undershot
        if
        (
               pos.second < target.second.first
            || (vel.first == 0 && pos.first < target.first.first)
        )
            return FireResult::under;

        // overshot
        if (pos.first > target.first.second)
            return FireResult::over;
    }
}

size_t valid_shots(const Range& target)
{
    size_t res = 0;

    for (Vel::second_type y = target.second.first; y != INT16_MAX; ++y)
    {
        for (Vel::first_type x = 0; x != target.first.second+1; ++x)
        {
            switch (fire({x,y}, target))
            {
                case FireResult::hit:
                    ++res;
                break;

                case FireResult::over:
                    // goto break_1;

                case FireResult::under:
                ;
            }
        }
        // break_1: ;
    }

    return res;
}

int main()
{
    // constexpr Range target = {{ 20, 30}, {- 10,-  5}};
    constexpr Range target = {{138,184}, {-125,-71}};

    std::cout << valid_shots(target) << std::endl;
}
